<head>
    <meta name="resource-type" content="document" />
    <meta http-equiv="Content-Language" content="hu, hun, hungarian">
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="country" content="Hungary">

    <?php
    if($this->uri->segment(3) == "")
        echo '<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>';
    
    if (isset($css_files)) {
        foreach ($css_files as $file):
            ?>
            <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
        <?php
        endforeach;
    }
    ?>

            
    <?php
    if (isset($js_files)) {
        foreach ($js_files as $file):
            ?>
            <script src="<?php echo $file; ?>"></script>
        <?php
        endforeach;
    } 
    ?>
 
    <!-- Bootstrap -->
    
    <link href="<?php echo base_url();?>bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'>
    <link href="<?php echo base_url();?>css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>css/admin_style.css" rel="stylesheet">
    <link href="<?php echo base_url();?>css/style-responsive.css" rel="stylesheet">
    <link href="<?php echo base_url();?>css/bootstrap.css" rel="stylesheet">
    
    <link href="<?php echo base_url();?>css/jquerysctipttop.css" rel="stylesheet" type="text/css">
    <script src="<?php echo base_url("js/jquery-ui-1.11.4.custom/jquery-ui.min.js");?>"></script>
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->


</head>