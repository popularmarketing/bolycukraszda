<?php include('header.php');?>
<?php include('primari.php');?>       
	<section class="top-section">
        	<div class="offset-borders">
                <div class="full-header-container" id="header-contact">
                    <div class="full-header">
                        <div class="container">
                            <h1>Elérhetőségünk</h1>
                        </div>
                    </div>
                </div>
            </div>
     	</section>
        
        <section class="contact-section">            
            <div class="container">
                <div class="content-box">
                    <!-- Google map -->
                    <div class="google-map-big-container onscroll-animate">
                        <div class="google-map">
                            <div id="map-canvas"></div>
                        </div>
                    </div>
                    <!-- /Google map -->
                    
                    <div class="row">
                        <div class="col-md-8 onscroll-animate">
                        	<article>
                                <div class="article-header">
                                    <h1>Hagyjon üzenetet</h1>
                                </div>
                                <form class="form-contact-alt" action="<?php echo base_url("oldal/sendmail");?>" method="post">
                                    <div class="row">
                                        <div class="col-sm-7">
                                            <input type="text" name="senderName">
                                        </div>
                                        <div class="col-sm-5 input-description">
                                            <i class="fa fa-user"></i> Név
                                        </div>
                                        <div class="col-sm-7">
                                            <input type="text" name="email">
                                        </div>
                                        <div class="col-sm-5 input-description">
                                            <i class="fa fa-envelope"></i> E-mail cím
                                        </div>
                                    </div>
                                    <textarea name="message"></textarea>
                                    <div class="clearfix">
                                        <div class="submit-container">
                                            <input type="submit" value="Üzenet küldése">
                                        </div>
                                    </div>
                                    <p class="return-msg"></p>
                                </form>
                            </article>
                        </div>
                        <div class="col-md-4 onscroll-animate" data-delay="500">
                        	<article>
                            	<div class="article-header">
                                	<h1>Elérhetőségünk</h1>
                                </div>
                                <div class="margin-20"></div>
                                <p><?php echo $beallitasok->vezetekes;?></p>
                                <p><?php echo $beallitasok->uzletcim;?></p>
                                <p><?php echo $beallitasok->nyilvanosemail;?></p>
								<?php print_r($oldal->tartalom);?>
                         	</article>
                            <div class="margin-10"></div>
                            <article>
                            	<div class="article-header">
                                	<h1>Szociális elérhetőségeink</h1>
                                </div>
                                <div class="social-icon-container-alt">
                                    <a href="elerhetosegunk"><i class="fa fa-facebook"></i></a>
                                </div>
                                <div class="social-icon-container-alt">
                                    <a href="elerhetosegunk"><i class="fa fa-twitter"></i></a>
                                </div>
                                <div class="social-icon-container-alt">
                                    <a href="elerhetosegunk"><i class="fa fa-rss"></i></a>
                                </div>
                                <div class="social-icon-container-alt">
                                    <a href="elerhetosegunk"><i class="fa fa-pinterest"></i></a>
                                </div>
                                <div class="social-icon-container-alt">
                                    <a href="elerhetosegunk"><i class="fa fa-tumblr"></i></a>
                                </div>
                                <div class="social-icon-container-alt">
                                    <a href="elerhetosegunk"><i class="fa fa-skype"></i></a>
                                </div>
                            </article>
                        </div>
                    </div>
                </div>
            </div>
        </section>
<?php include('footer.php');?>