<?php include('header.php');?>
<?php include('primari.php');?>
<section class="top-section">
        	<div class="offset-borders">
                <div class="full-header-container" id="header-gallery">
                    <div class="full-header">
                        <div class="container">
                            <h1>Galéria</h1>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        <section id="products-section">
            <div class="section-content">
                <div class="container">
                    <header class="section-header">
                        <h1>Legújabb termékeink</h1>
                        <p>Nézze meg a képeket a termékeinkről</p>
                    </header>
                	<div id="products-slider-1" class="products-slider">
                        <div class=""><!-- slide 1 -->
                            <div class="row">
                                <div class="col-md-12 onscroll-animate" data-delay="300">
									<?php $count=1;  foreach($galeria->result() as $row){
										if ($count%4 == 1)
										{  
											 ?><div class="row"><?php
										}
										?>
										<div class="col-sm-3">
											<div class="product">
													<div class="product-preview">
														<a href="<?php echo base_url('/assets/uploads/files/'.$row->file)?>" data-lity><img alt="product" src="<?php echo base_url("/assets/uploads/files/".$row->file)?>"></a>
													</div>
											</div><!-- .product -->
										</div>
										<?php
										if ($count%4 == 0)
										{
											?></div><?php
										}
										$count++;
									?>		
										<?php }?>
                                </div>
                            </div><!-- .row -->
                        </div>
                    </div><!-- .products-slider -->
                </div><!-- .container -->
            </div><!-- .section-content -->
		</section>
<?php include('footer.php');?>