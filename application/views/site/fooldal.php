<?php include('header.php');?>
<?php include('primari.php');?>
<?php include('slider.php');?>
        <section id="products-section">
            <div class="section-content">
                <div class="container">
                    <header class="section-header">
                        <h1>Pár új sütemény</h1>
                        <p>Tekintse meg a képeket süteményinkről</p>
                    </header>
                    
                    <div id="products-slider-1" class="products-slider">
                        <div><!-- slide 1 -->
                            <div class="row">
                                <div class="col-md-12 onscroll-animate">
										<?php  foreach($galeria->result() as $row){ ?> 
											  <div class="col-sm-3">
                                            <div class="product">
                                                <div class="product-preview">
                                                    <a href="galeria"><img alt="product" src="<?php echo base_url("/assets/uploads/files/".$row->file)?>"></a>
                                                </div>
                                            </div><!-- .product -->
                                        </div>
										<?php }?>
                                </div><!-- .col-md-6 -->
                            </div><!-- .row -->
                        </div><!-- .slide-1 -->
                    </div><!-- .products-slider -->
                    
                    <p class="text-center onscroll-animate">
                        <a href="galeria" class="button-void">Tovább a galériához</a>
                    </p>
                </div><!-- .container -->
            </div><!-- .section-content -->
        </section>
                
        <section id="offer-section">
            <div class="section-content">
                <div class="container">
                    <header class="section-header onscroll-animate">
                        <h1>Heti ajánlat</h1>
                        <p>
                            Nézze meg, ezen a héten mit ajánlunk az ön számára
                        </p>
                    </header>
                    <?php foreach($ajanlat->result() as $row){ ?>
                    <div class="tabs-big-container centered-columns onscroll-animate">
                        <div class="centered-column tab-content centered-column-top">
                        <!-- Tab panes -->
                            <div role="tabpanel" class="tab-pane fade in active" id="popular">
                                <article>
                                    <div class="centered-columns offer-box">
                                        <div class="offer-box-left centered-column">
                                            <div class="offer-info">
                                                <h1><?php echo $row->nev?></h1>
                                                <p><?php echo $row->hosszuNev; ?></p>
                                                
                                                <div class="margin-100"></div>
                                                <h2 class="text-huge"><?php echo number_format($row->ar); ?>Ft</h2>
                                                <div class="margin-20"></div>
                                            </div>
                                        </div>
                                        <div class="offer-box-right centered-column" style="background-image:url('assets/uploads/files/<?php echo $row->file; ?>');">
                                        </div>
                                    </div><!-- .row -->
                                </article>
                            </div><!-- .tab-pane -->
                        </div><!-- .centered-column -->
                    </div><!-- .tabs-big-container -->
                    <?php } ?>
                    <div class="margin-80"></div>
                </div><!-- .container -->
            </div><!-- .section-content -->
        </section>
                
        <section id="contact-section">
            <div class="section-content">
            
                <!-- Google map -->
                <div class="google-map onscroll-animate">
                    <div id="map-canvas"></div>
                </div>
                <!-- /Google map -->
                
                <div class="container">
                    <form class="form-contact onscroll-animate" action="oldal/sendmail" method="post">
                        <h2>Írjon nekünk</h2>
                        <p>Írjon nekünk valamit és válaszolni fogunk.</p>
                        <input type="text" name="senderName" placeholder="Teljes név">
                        <input type="text" name="email" placeholder="E-mail cím">
                        <textarea name="message" placeholder="Üzenet"></textarea>
                        <div class="text-center">
                            <input type="submit" value="Üzenet küldése">
                        </div>
                    </form>
                </div>
                
            </div><!-- .section-content -->
        </section>
<?php include('footer.php');?>