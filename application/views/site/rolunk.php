<?php include('header.php');?>
<?php include('primari.php');?>
		<section class="top-section">
        	<div class="offset-borders">
                <div class="full-header-container" id="header-about">
                    <div class="full-header">
                        <div class="container">
                            <h1>Rólunk</h1>
                        </div>
                    </div>
                </div>
            </div>
        </section>

		<section id="quote-section">            
            <div class="container">
               	<div class="quote">
                   	1991 óta működő cukrászat családi vállalkozás, melyben szakképzett cukrászok készítik el a süteményeket, tortákat! 
                </div>
            </div>
      	</section>
        
        <section id="about-section">
        	<div class="section-content">
                <div class="container">
                    <header class="section-header">
                        <h1>A Cukrászdáról</h1>
                    </header>
                    
                    <div class="row">
                    	<div class="col-md-6 onscroll-animate">
                        	<img class="img-responsive" alt="cup and logo" src="images/services4.jpg">
                        </div>
                        <div class="col-md-6 onscroll-animate" data-delay="400">
                        	<article>
                            	<?php print_r($oldal->tartalom); ?>
                            </article>
                            <div class="margin-70"></div>
                        </div>
                    </div>
              	</div>
          	</div>
      	</section>
<?php include('footer.php');?>