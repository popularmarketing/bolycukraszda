        <footer class="page-footer">
            <div class="footer-dark">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
								<div class="col-sm-3"></div>
                                <div class="col-sm-6 footer-column onscroll-animate">
                                    <h4>Nyitvatartás</h4>
                                    <?php print_r($beallitasok->nyitvatartas);?>
                                    <div class="margin-40"></div>
                                </div>
								<div class="col-sm-3"></div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
								<div class="col-sm-3"></div>
                                <div class="col-sm-6 footer-column onscroll-animate" data-delay="500">
                                    <h4>Elérhetőségünk</h4>
                                    <div class="icon-opening-wrapper">
                                        <div class="icon-opening-container">
                                            <p class="icon-opening"><i class="fa fa-phone"></i></p>
                                            <p class="icon-opening-content"><?php echo $beallitasok->vezetekes?></p>
                                        </div>
                                    </div>
                                    <div class="icon-opening-wrapper">
                                        <div class="icon-opening-container">
                                            <p class="icon-opening"><i class="fa fa-location-arrow"></i></p>
                                            <p class="icon-opening-content">7754 Bóly Ady<br> Endre utca 5.</p>
                                        </div>
                                    </div>
                                    <div class="icon-opening-wrapper">
                                        <div class="icon-opening-container">
                                            <p class="icon-opening"><i class="fa fa-envelope"></i></p>
                                            <p class="icon-opening-content"><?php echo $beallitasok->nyilvanosemail?></p>
                                        </div>
                                    </div>
                                    <div class="margin-40"></div>
                                </div>
								<div class="col-sm-3"></div>
                            </div>
                        </div>
                    </div>
                    
                    <p class="site-info">2016 Minden jog fenntartva - Az oldalt készítette <a href="http://popularmarketing.hu" target="_blank">PopularMarketing.hu</a></p>
                	<a href="#all" class="to-top scroll-to"></a>
                </div>
            </div><!-- .footer-darl -->
        </footer>
    </div>
    
	<script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
    <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="owl-carousel/owl.carousel.min.js"></script>
	<script type="text/javascript" src="masterslider/masterslider.min.js"></script>
    <script type="text/javascript" src="js/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="js/jquery.stellar.min.js"></script>
    <script type="text/javascript" src="js/placeholder-fallback.js"></script>
    <script type="text/javascript" src="js/jquery.inview.min.js"></script>
	<script type="text/javascript" src="js/custom.js"></script>
	
	<script src="js/lity.js"></script>
    
</body>
</html>