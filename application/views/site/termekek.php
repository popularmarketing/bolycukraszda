<?php include('header.php');?>
<?php include('primari.php');?>
        <section class="top-section">
        	<div class="offset-borders">
                <div class="full-header-container" id="header-menus">
                    <div class="full-header">
                        <div class="container">
                            <h1>Termékeink</h1>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        <section id="products-section">
            <div class="section-content">
                <div class="container">
                    <header class="section-header">
                        <h1>Választékunk</h1>
                    </header>
                    
                    <div class="text-center onscroll-animate">
                    	<div class="filter-icons-container">
										<?php foreach($kategoriak->result() as $row){ ?>
											<div class="filter-icon-wrapper">
												<a href="termekek/<?php echo $row->id?>">
													<div class="filter-icon-content">
														<div class="filter-icon">
															<?php print_r($row->header_custom_code);?>
														</div>
													</div>
												</a>
											</div>
										<?php } ?>           
                        </div>
                    </div>
                    
                    <div class="margin-20"></div>
                    
                 	<div class="row">
                        <div class="col-md-12 onscroll-animate">
							<?php $count=1;  foreach($termekek->result() as $row){
									if($filter==$row->kategoria){
										if ($count%4 == 1)
										{  
											 ?><div class="row"><?php
										}
										?>
										    <div class="col-sm-3">
												<div class="product">
													<div class="product-preview">
														<img alt="product 2" src="<?php echo base_url("/assets/uploads/termekek/".$row->fokep);?>">
													</div>
													<div class="product-detail-container">
														<div class="product-detail">
															<h2><a href="gallery_single.html"><?php echo $row->nev;?></a></h2>
															<p><?php echo $row->leiras;?></p>
															<p class="product-price"><?php echo $row->ar;?>Ft</p>
														</div>
													</div><!-- .product-detail-container -->
												</div><!-- .product -->
											</div>
										<?php
										if ($count%4 == 0)
										{
											?></div><?php
										}
										$count++;
										}
									?>		
										<?php }?>
	
                        </div><!-- .col-md-6 -->
                    </div><!-- .row -->
                        </div><!-- .col-md-6 -->
                    </div><!-- .row -->
        </section>
<?php include('footer.php');?>