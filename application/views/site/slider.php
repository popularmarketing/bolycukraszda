<section id="slider-container" class="top-section">
            <div class="offset-borders">
                <div class="ms-fullscreen-template">
                    <div class="master-slider ms-skin-round" id="masterslider">
                        
							<?php
								foreach($slider->result() as $row)
									{ ?>
									<div class="ms-slide">
										<img src="<?php echo base_url("assets/uploads/slider/".$row->file)?>" alt="header img">
										<div class="ms-layer">
											<h2 class="ms-layer" data-type="text" data-effect="rotate3dbottom(30,0,0,70)">Breitenstein Cukrászda</h2>
										</div>
								 </div>
							   <?php };?> 
                    </div><!-- .master-slider -->
                </div><!-- .ms-fullscreen-template -->
        	</div>
        </section>