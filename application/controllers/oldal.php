<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Oldal extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
        $this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");


        $this->load->model("Lekerdezes");
        $this->load->model("Alapfunction");
        $this->load->helper("url");
        $this->load->library("pagination");

    }

    public function index()
    {
            $nyelv = $this->input->get('lang', TRUE);
            $data['nyelv'] = $this->Alapfunction->nyelv($nyelv);
			$data['oldal'] = $this->Lekerdezes->oldal('fooldal');
			$data['beallitasok'] = $this->Lekerdezes->beallitasok();
			$data['slider'] = $this->Lekerdezes->slider($data['nyelv']);
			$data['galeria'] = $this->db->query('SELECT * FROM galeria ORDER BY id DESC limit 0,4');
			$data['ajanlat'] = $this->db->query('SELECT * FROM ajanlat limit 0,1');
            $this->load->view("site/fooldal",$data);
    }

    public function url()
    {
        $nyelv = $this->input->get('lang', TRUE);
		
		$url = $this->uri->segment(1);
		$filter = $this->uri->segment(2);
		$filter = (int)$filter;
		if(!isset($filter)){
			$filter=3;
		}
		
		$data['nyelv'] = $this->Alapfunction->nyelv($nyelv);
        $data['oldal'] = $this->Lekerdezes->oldal($url);
		$data['beallitasok'] = $this->Lekerdezes->beallitasok();
		$data['galeria'] = $this->db->query('SELECT * FROM galeria');
		$data['kategoriak'] = $this->db->query('SELECT * FROM kategoriak ORDER BY sorrend ASC');
		$data['termekek'] = $this->db->query('SELECT * FROM termekek');
		$data['filter']= $filter;

        $this->load->view("site/".$url,$data);
    }
 
	public function sendmail()
     {
         
            $this->load->helper('url');
            
            $nyelv = $this->input->get('lang', TRUE);
            $data['nyelv'] = $this->Alapfunction->nyelv($nyelv);
            $beallitasok = $this->Lekerdezes->beallitasok(" WHERE nyelv='".$data['nyelv']."' ");
            
           
            $senderName = $this->input->post("senderName");
            $email = $this->input->post("email");
            $message = $this->input->post("message");          
          
            $this->load->library('email');
         
            $this->email->from($email ,$senderName);
            $this->email->to($beallitasok->nyilvanosemail); 

            $this->email->subject('E-mail a weboldalról');
            $this->email->message($message);    

            $this->email->send();
            
            redirect($uri='./');

         
     }
}

