<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bejelentkezes extends CI_Controller {

    public function index()
    {

        $data['hiba'] = $this->input->get('par');
        $perm = $this->session->userdata('perm');
        //ha már be van jelentkezve átirányítjuk ahova kell
        if($perm == 1)
        {
            redirect(base_url("admin"), 'refresh');
        }


        $this->load->view('header');
        $this->load->view('login',$data);
        $this->load->view('footer');

    }


    public function check()
    {
	$name = $this->input->post("name");
        $password = $this->input->post("password");
        $this->load->model('usercheck');
        $login = $this->usercheck->login($name,$password);
     
        $hiba = urlencode("Hibás felhasználónév vagy jelszó!");

        if ($login === FALSE)
        {
            redirect($base_url.'bejelentkezes?par='.$hiba, 'refresh');
        }else{
            redirect('/admin', 'refresh');
        }

    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */

?>