-- phpMyAdmin SQL Dump
-- version 4.4.7
-- http://www.phpmyadmin.net
--
-- Gép: localhost:3306
-- Létrehozás ideje: 2016. Már 03. 11:55
-- Kiszolgáló verziója: 5.6.26
-- PHP verzió: 5.5.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Adatbázis: `alapwebsite`
--

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `ajanlat`
--

CREATE TABLE IF NOT EXISTS `ajanlat` (
  `id` int(11) NOT NULL,
  `nev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `hosszuNev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `file` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `ar` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `ajanlat`
--

INSERT INTO `ajanlat` (`id`, `nev`, `hosszuNev`, `file`, `ar`) VALUES
(3, 'Születésnapi torta', 'Sokféle születésnapi tortát készít cégünk, közöttük a csokis, marcipános és gyümölcsös. Célunk, a kinézet és az íz maximalizálása és a mosolyok csalása az emberek arcára.', '8e305-offer1.png', 2000);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `beallitasok`
--

CREATE TABLE IF NOT EXISTS `beallitasok` (
  `id` int(11) NOT NULL,
  `oldalnev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `logo` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `favicon` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `mobil` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `vezetekes` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `fax` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `adoszam` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `telephelycim` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `uzletcim` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `nyitvatartas` text COLLATE utf8_hungarian_ci NOT NULL,
  `adminemail` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `nyilvanosemail` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `nyitva` int(1) NOT NULL,
  `nyelv` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `fooldal_title` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `fooldal_keywords` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `fooldal_description` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `google_analytics` varchar(256) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `beallitasok`
--

INSERT INTO `beallitasok` (`id`, `oldalnev`, `logo`, `favicon`, `mobil`, `vezetekes`, `fax`, `adoszam`, `telephelycim`, `uzletcim`, `nyitvatartas`, `adminemail`, `nyilvanosemail`, `nyitva`, `nyelv`, `fooldal_title`, `fooldal_keywords`, `fooldal_description`, `google_analytics`) VALUES
(1, 'Breitenstein Cukrászda', '', '', '', '0669/368-392', '', '', '', '7754 Bóly Ady Endre utca 5.', '<p>\r\n	H&eacute;tfő-P&eacute;ntek: <span class="highlight">08:00 - 20:30</span><br />\r\n	Szombat: <span class="highlight">10:00 - 16:30 </span><br />\r\n	Vas&aacute;rnap: <span class="highlight">Z&aacute;rva</span></p>\r\n', '', 'kox30@freemail.hu', 1, 'hu', '', '', '', ''),
(2, '', '', '', '', '', '', '', '', '', '', '', '', 0, 'de', '', '', '', ''),
(3, '', '', '', '', '', '', '', '', '', '', '', '', 0, 'en', '', '', '', ''),
(4, '', '', '', '', '', '', '', '', '', '', '', '', 0, 'cz', '', '', '', '');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `companytype`
--

CREATE TABLE IF NOT EXISTS `companytype` (
  `id` int(11) NOT NULL,
  `name` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `link1` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `link2` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `link3` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `link4` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `link5` varchar(256) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `companytype`
--

INSERT INTO `companytype` (`id`, `name`, `link1`, `link2`, `link3`, `link4`, `link5`) VALUES
(1, 'Étterem', '', '', '', '', ''),
(2, 'Fodrászat', '', '', '', '', ''),
(3, 'Cukrászda/Fagyizó', '', '', '', '', ''),
(4, 'Fitnesz / Gym / Edzőterem', '', '', '', '', ''),
(5, 'Tetóválószalon', '', '', '', '', ''),
(6, 'Plasztikai Sebészet', '', '', '', '', ''),
(7, 'Esküvői szalon', '', '', '', '', ''),
(8, 'Sexshop', '', '', '', '', ''),
(9, 'Ipari cikk', '', '', '', '', ''),
(10, 'Kozmetika', '', '', '', '', ''),
(11, 'Ruhazati boltok', '', '', '', '', ''),
(12, 'Utazasi iroda', '', '', '', '', ''),
(13, 'Konyveloi iroda', '', '', '', '', ''),
(14, 'Nyelviskola', '', '', '', '', ''),
(15, 'Fogászat', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `dokumentumok`
--

CREATE TABLE IF NOT EXISTS `dokumentumok` (
  `dokumentumokid` int(11) NOT NULL,
  `name` varchar(500) NOT NULL,
  `url` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `galeria`
--

CREATE TABLE IF NOT EXISTS `galeria` (
  `id` int(11) NOT NULL,
  `image_url` varchar(256) NOT NULL,
  `file` varchar(256) NOT NULL,
  `nev` varchar(256) NOT NULL,
  `active` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `galeria`
--

INSERT INTO `galeria` (`id`, `image_url`, `file`, `nev`, `active`) VALUES
(6, '', '8caa8-alkalmi-1_01.jpg', '', 2),
(7, '', '61f63-ing_01.jpg', '', 2),
(8, '', 'b77df-maci_01.jpg', '', 2),
(9, '', 'b2c0b-p5240007.jpg', '', 2),
(10, '', '4b7cf-torta2.jpg', '', 2),
(11, '', '78667-torta3.jpg', '', 2);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `gyarto`
--

CREATE TABLE IF NOT EXISTS `gyarto` (
  `id` int(11) NOT NULL,
  `nev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `url` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `sorrend` int(10) NOT NULL,
  `statusz` int(1) NOT NULL,
  `fokep` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `title` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `keywords` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `description` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `header_custom_code` text COLLATE utf8_hungarian_ci NOT NULL,
  `nyelv` varchar(5) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `gyik`
--

CREATE TABLE IF NOT EXISTS `gyik` (
  `cim` varchar(1000) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `tartalom` text CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `sorrend` int(255) NOT NULL,
  `tag` varchar(255) NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `hirek`
--

CREATE TABLE IF NOT EXISTS `hirek` (
  `id` int(11) NOT NULL,
  `url` varchar(256) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `nev` varchar(256) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `kategoria` int(11) NOT NULL,
  `lead` text CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `tartalom` text CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `datum` date NOT NULL,
  `statusz` int(11) NOT NULL,
  `kiemelt` int(11) NOT NULL,
  `fokep` varchar(256) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `tag` varchar(256) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `videoid` varchar(256) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `user` int(11) NOT NULL,
  `title` varchar(256) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `keywords` varchar(256) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `description` varchar(500) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `header_custom_code` varchar(500) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `nyelv` varchar(5) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `hirek_kategoria`
--

CREATE TABLE IF NOT EXISTS `hirek_kategoria` (
  `id` int(11) NOT NULL,
  `nev` varchar(256) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `url` varchar(256) NOT NULL,
  `nyelv` varchar(5) NOT NULL,
  `fokep` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `kategoria`
--

CREATE TABLE IF NOT EXISTS `kategoria` (
  `kategoriaid` int(11) NOT NULL,
  `nev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `tipus` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `icon` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `markericon` varchar(128) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `kategoriak`
--

CREATE TABLE IF NOT EXISTS `kategoriak` (
  `id` int(11) NOT NULL,
  `nev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `url` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `szulo` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `sorrend` int(10) NOT NULL,
  `statusz` int(1) NOT NULL,
  `fokep` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `title` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `keywords` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `description` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `header_custom_code` text COLLATE utf8_hungarian_ci NOT NULL,
  `nyelv` varchar(5) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `kategoriak`
--

INSERT INTO `kategoriak` (`id`, `nev`, `url`, `szulo`, `sorrend`, `statusz`, `fokep`, `title`, `keywords`, `description`, `header_custom_code`, `nyelv`) VALUES
(1, 'TORTAK', 'tortak', '', 4, 1, '', '', '', '', '<p>\r\n	<svg enable-background="new 0 0 1000 1000" height="1000px" version="1.1" viewbox="0 0 1000 1000" width="1000px" x="0px" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" y="0px"> <path clip-rule="evenodd" d="M876.669,598.186c-203.68,79.196-554.465,82.353-763.607,6.949\r\n                                                    c-0.125,21.826-6.734,37.066-6.859,58.893C60.189,685.944,2.331,705.249,0.059,757.577\r\n                                                    c-3.157,72.814,120.998,115.854,198.604,135.136c177.391,44.106,428.312,42.063,602.647,0\r\n                                                    c77.039-18.579,201.591-59.392,198.638-135.136c-2.339-59.505-69.815-70.656-109.607-100.478\r\n                                                    C886.571,636.682,883.573,615.469,876.669,598.186z" fill-rule="evenodd"></path> <g> <g> <path clip-rule="evenodd" d="M595.858,300.917C561.973,263.511,571.034,346,595.858,300.917L595.858,300.917z\r\n                                                             M318.513,307.844c-19.907,42.812,47.332,34.681,34.238,3.475c129.537-6.223,276.471-27.209,349.263-90.098\r\n                                                            c20.44-17.647,41.608-45.583,37.679-76.221c-13.877-17.147-34.84-25.028-58.21-27.731\r\n                                                            c-144.039-16.67-360.517,42.971-407.485,107.427c-9.789,13.423-16.432,34.84-13.695,55.417\r\n                                                            C274.929,294.195,291.906,305.891,318.513,307.844z M462.324,349.43C527.224,391.583,481.152,269.802,462.324,349.43\r\n                                                            L462.324,349.43z M804.762,103.415c0.728-3.884,3.793-5.405,6.837-6.927c3.384-21.349-13.695-23.847-23.961-13.877\r\n                                                            C786.956,95.988,793.065,102.529,804.762,103.415z M746.529,228.148c9.356,2.521,6.904-6.881,13.718-6.927\r\n                                                            C763.427,180.613,716.776,213.908,746.529,228.148z M910.917,68.779c3.226,10.584,16.648,10.856,17.103,24.234\r\n                                                            C871.354,155.38,805.943,239.141,715.731,293.99c-84.374,51.283-195.367,102.953-304.77,110.879\r\n                                                            c-122.485,8.88-231.888-34.727-291.064-93.55c-16.761-16.67-20.384-41.994-41.086-51.987\r\n                                                            c6.95,48.126,51.215,86.487,92.449,114.354c69.953,47.264,164.434,69.521,270.521,58.915\r\n                                                            C667.81,410.002,852.593,280.658,951.98,141.525c5.678,0.068,1.931,9.607,6.858,10.402\r\n                                                            C959.975,111.25,922.228,73.799,910.917,68.779z M818.435,318.247c-31.524,34.522-4.179,119.328-51.352,124.733\r\n                                                            c-43.198,4.974-43.13-53.623-85.601-58.892c-17.852,49.171-17.171,137.475-85.624,107.405\r\n                                                            c-26.913-11.833-19.169-56.394-54.781-65.842c-31.059,22.894-22.894,75.517-58.21,86.646\r\n                                                            c-47.423,14.922-64.241-48.104-102.726-69.317c-15.444,30.797-17.102,71.452-51.363,72.769\r\n                                                            c-47.138,1.816-40.756-63.139-75.324-86.623c-23.427,16.057-14.695,51.76-44.515,55.439\r\n                                                            c-44.039,5.428-50.682-50.375-51.375-103.952c-18.839,10.97-16.602,43.266-41.085,48.513v83.171\r\n                                                            c213.775,70.065,553.283,47.263,749.899-20.804c-3.61-52.283-2.906-106.792,13.695-159.392\r\n                                                            C861.769,326.854,843.781,305.096,818.435,318.247z M229.482,314.794c14.922-22.099-25.698-36.339-20.542-3.475\r\n                                                            C212.982,315.294,221.033,315.249,229.482,314.794z M250.025,193.513c-12.81-14.922-41.994-5.996-34.238,20.781\r\n                                                            C227.949,228.535,259.132,221.653,250.025,193.513z" fill-rule="evenodd"></path> </g> </g> <path clip-rule="evenodd" d="M119.897,713.251c-4.531,18.328-30.218,9.925-41.086,13.877\r\n                                                    C70,796.785,151.558,828.877,215.787,848.387c154.179,46.877,389.746,48.558,558.132,3.475\r\n                                                    c71.884-19.236,166.297-46.809,157.53-128.208c-12.787-14.014-51.466-5.02-34.25-45.038c42.153,13.945,72.042,40.291,68.499,100.478\r\n                                                    c-118.42,162.89-681.094,170.316-876.609,55.462c-24.313-14.285-57.029-46.286-58.21-72.791\r\n                                                    C28.925,718.202,106.043,641.391,119.897,713.251z" fill="#FFFFFF" fill-rule="evenodd"></path> <path clip-rule="evenodd" d="M893.793,730.58c-14.49,20.123-48.331,3.543-34.25-24.233\r\n                                                    C877.871,696.513,898.153,700.487,893.793,730.58z" fill="#FFFFFF" fill-rule="evenodd"></path> <path clip-rule="evenodd" d="M123.327,758.312c-21.656-17.942-5.576-46.083,13.695-45.061\r\n                                                    C162.369,714.613,169.478,765.942,123.327,758.312z" fill="#FFFFFF" fill-rule="evenodd"></path> <path clip-rule="evenodd" d="M856.113,737.508c-0.158,16.034-9.721,22.507-27.391,20.804\r\n                                                    C800.992,740.414,844.099,704.62,856.113,737.508z" fill="#FFFFFF" fill-rule="evenodd"></path> <path clip-rule="evenodd" d="M154.147,751.385c4.622-3.407,6.848-9.267,10.277-13.877\r\n                                                    c6.836,0,13.695,0,20.543,0C212.017,768.44,156.679,791.448,154.147,751.385z" fill="#FFFFFF" fill-rule="evenodd"></path> <path clip-rule="evenodd" d="M811.599,744.435c16.193,29.548-32.092,37.974-30.82,6.95\r\n                                                    C785.934,741.459,799.834,738.348,811.599,744.435z" fill="#FFFFFF" fill-rule="evenodd"></path> <path clip-rule="evenodd" d="M229.482,744.435c17.863,15.194,9.187,42.039-6.848,45.061\r\n                                                    C192.132,795.241,178.392,736.917,229.482,744.435z" fill="#FFFFFF" fill-rule="evenodd"></path> <path clip-rule="evenodd" d="M780.778,765.238c0.159,10.562-7.223,13.469-10.266,20.781\r\n                                                    c-16.33,2.681-22.575-4.883-27.391-13.854C741.669,747.887,775.305,745.524,780.778,765.238z" fill="#FFFFFF" fill-rule="evenodd"></path> <path clip-rule="evenodd" d="M277.416,789.495c-14.899,4.86-21.667,8.357-34.238,0\r\n                                                    C231.197,743.73,295.392,755.2,277.416,789.495z" fill="#FFFFFF" fill-rule="evenodd"></path> <path clip-rule="evenodd" d="M729.404,761.764c3.906,7.608,8.857,14.172,6.858,27.731\r\n                                                    c-5.519,7.108-17.17,8.017-30.797,6.927C694.519,781.205,700.719,755.745,729.404,761.764z" fill="#FFFFFF" fill-rule="evenodd"></path> <path clip-rule="evenodd" d="M321.931,792.947c-6.019,9.425-28.219,9.425-34.238,0\r\n                                                    C279.834,757.471,329.789,757.471,321.931,792.947z" fill="#FFFFFF" fill-rule="evenodd"></path> <path clip-rule="evenodd" d="M695.178,796.422c-5.542,5.95-14.332,8.607-27.414,6.927\r\n                                                    C642.35,776.526,701.9,748.613,695.178,796.422z" fill="#FFFFFF" fill-rule="evenodd"></path> <path clip-rule="evenodd" d="M363.028,779.093c0,8.085,0,16.171,0,24.256\r\n                                                    c-9.63,4.815-19.691,9.153-30.82,3.476C316.4,781.932,345.12,761.128,363.028,779.093z" fill="#FFFFFF" fill-rule="evenodd"></path> <path clip-rule="evenodd" d="M650.662,775.618c3.884,7.631,8.835,14.172,6.836,27.73\r\n                                                    C631.539,838.075,595.041,766.987,650.662,775.618z" fill="#FFFFFF" fill-rule="evenodd"></path> <path clip-rule="evenodd" d="M404.114,782.567c11.867,22.395-13.945,39.405-30.82,24.257\r\n                                                    C365.674,781.772,387.364,771.848,404.114,782.567z" fill="#FFFFFF" fill-rule="evenodd"></path> <path clip-rule="evenodd" d="M606.146,779.093c7.019,5.587,7.927,17.374,6.837,31.184\r\n                                                    C584.071,840.506,558.611,770.371,606.146,779.093z" fill="#FFFFFF" fill-rule="evenodd"></path> <path clip-rule="evenodd" d="M448.629,789.495c4.122,25.937-11.913,34.567-30.82,27.708\r\n                                                    C400.65,793.628,432.015,768.781,448.629,789.495z" fill="#FFFFFF" fill-rule="evenodd"></path> <path clip-rule="evenodd" d="M489.726,806.824c-4.486,11.333-26.8,14.422-34.25,3.452\r\n                                                    C446.312,775.595,495.313,774.459,489.726,806.824z" fill="#FFFFFF" fill-rule="evenodd"></path> <path clip-rule="evenodd" d="M534.241,789.495c3.066,20.44-5.155,29.435-20.555,31.183\r\n                                                    C482.946,811.162,510.2,762.104,534.241,789.495z" fill="#FFFFFF" fill-rule="evenodd"></path> <path clip-rule="evenodd" d="M571.92,789.495c3.589,22.121-5.541,31.364-27.413,27.708\r\n                                                    C526.814,797.875,552.093,769.939,571.92,789.495z" fill="#FFFFFF" fill-rule="evenodd"></path> </svg></p>\r\n', ''),
(2, 'KENYERFELEK', 'kenyerfelek', '', 3, 1, '', '', '', '', '<p>\r\n	<svg enable-background="new 0 0 1000 1000" height="1000px" version="1.1" viewbox="0 0 1000 1000" width="1000px" x="0px" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" y="0px"> <path clip-rule="evenodd" d="M387.273,318.427c-24.886,8.352-57.601,49.199-79.439,77.621\r\n                                                    c-23.69,30.829-47.38,82.2-79.438,80.297c-26.283-1.549-46.623-33.304-43.575-61.557c3.047-28.169,30.257-37.093,33.321-56.203\r\n                                                    C154.328,372.627,90.8,412.464,49.01,462.959c-28.186,34.063-73.023,118.956-33.321,173.963\r\n                                                    c44.804,62.098,228.197,48.189,351.076,48.189c144.212,0,300.21,0.809,407.448,2.678c38.911,0.672,94.845,3.688,130.691,0\r\n                                                    c65.633-6.787,115.05-75.617,87.134-147.21c-12.241-31.352-42.936-47.785-69.185-74.926c-39.215,3.064-76.206,69.672-112.76,69.572\r\n                                                    c-29.785-0.067-52.382-25.997-48.694-58.88c4.748-42.261,56.17-48.424,76.88-80.297c-49.906-36.116-103.836,7.627-143.505,48.188\r\n                                                    c-28.203,28.825-116.784,126.819-130.692,40.141c-13.537-84.389,77.436-113.383,107.625-155.224\r\n                                                    c-12.156-3.368-26.637-4.293-38.423-8.032c-40.09,24.212-75.836,33.069-112.76,66.912\r\n                                                    c-25.391,23.235-68.191,108.651-107.625,109.729c-26.384,0.708-51.405-25.07-51.253-53.525\r\n                                                    c0.202-42.43,96.023-108.635,97.371-133.824C437.785,317.99,408.656,311.271,387.273,318.427z" fill-rule="evenodd"></path> </svg></p>\r\n', ''),
(3, 'PEKSUTEMENYEK', 'peksutemenyek', '', 2, 1, '', '', '', '', '<p>\r\n	<svg enable-background="new 0 0 1000 1000" height="1000px" version="1.1" viewbox="0 0 1000 1000" width="1000px" x="0px" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" y="0px"> <path clip-rule="evenodd" d="M686.344,504.227c6.836-54.001,35.174-164.385-6.219-193.098\r\n                                                    c-28.045-19.479-110.537-9.337-151.996-9.337c-49.582,0-116.158-7.521-164.437,0c-91.452,14.237-52.904,137.486-40.33,211.772\r\n                                                    c6.596,38.924,12.473,137.007,40.33,155.732c47.868,32.174,198.272-12.232,266.817,9.354\r\n                                                    C677.659,650.21,676.836,579.403,686.344,504.227z M220.981,357.849C148.923,389.184,23.24,489.647,3.813,572.739\r\n                                                    C-13.902,648.6,32.098,699.38,99.993,700.443c62.858,0.977,172.299,7.589,204.765-28.028\r\n                                                    c-15.881-104.49-18.708-222.086-46.531-314.566C243.356,352.623,228.999,354.371,220.981,357.849z M925.221,697.325\r\n                                                    c111.838-48.896,80.195-161.489,21.723-224.228c-52.287-56.125-136.578-116.105-201.646-105.912\r\n                                                    c-19.857,41.289-21.553,98.682-27.926,152.614c-6.477,54.67-12.85,112.491-12.404,161.952\r\n                                                    C753.006,712.418,853.024,690.9,925.221,697.325z" fill-rule="evenodd"></path> </svg></p>\r\n', '');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `kerdezzfelelek`
--

CREATE TABLE IF NOT EXISTS `kerdezzfelelek` (
  `id` int(11) NOT NULL,
  `nev` varchar(256) CHARACTER SET utf32 COLLATE utf32_hungarian_ci NOT NULL,
  `email` varchar(256) NOT NULL,
  `kerdes` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_hungarian_ci NOT NULL,
  `datum` datetime NOT NULL,
  `kitol` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `marka`
--

CREATE TABLE IF NOT EXISTS `marka` (
  `id` int(11) NOT NULL,
  `nev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `url` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `sorrend` int(10) NOT NULL,
  `statusz` int(1) NOT NULL,
  `fokep` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `title` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `keywords` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `description` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `header_custom_code` text COLLATE utf8_hungarian_ci NOT NULL,
  `nyelv` varchar(5) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `megjelenes`
--

CREATE TABLE IF NOT EXISTS `megjelenes` (
  `megjelenesid` int(11) NOT NULL,
  `ugyfelszam` varchar(128) COLLATE utf8_hungarian_ci NOT NULL,
  `cegnev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `iranyitoszam` varchar(128) COLLATE utf8_hungarian_ci NOT NULL,
  `varos` varchar(128) COLLATE utf8_hungarian_ci NOT NULL,
  `cim` varchar(400) COLLATE utf8_hungarian_ci NOT NULL,
  `bevezeto` text COLLATE utf8_hungarian_ci NOT NULL,
  `kategoria` varchar(128) COLLATE utf8_hungarian_ci NOT NULL,
  `szoveg` text COLLATE utf8_hungarian_ci NOT NULL,
  `szoveg2` text COLLATE utf8_hungarian_ci NOT NULL,
  `youtubeid` varchar(128) COLLATE utf8_hungarian_ci NOT NULL,
  `fokep` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `kep1` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `kep2` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `kep3` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `kep4` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `kep5` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `telefonszam` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `email` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `mobil` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `webcim` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `nyitvatartas` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `statusz` int(1) NOT NULL,
  `letrehozva` datetime NOT NULL,
  `lejarat` datetime NOT NULL,
  `gpsx` varchar(128) COLLATE utf8_hungarian_ci NOT NULL,
  `gpsy` varchar(128) COLLATE utf8_hungarian_ci NOT NULL,
  `url` varchar(128) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `nyelvi_forditasok`
--

CREATE TABLE IF NOT EXISTS `nyelvi_forditasok` (
  `id` int(11) NOT NULL,
  `azonosito` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `ertek` text COLLATE utf8_hungarian_ci NOT NULL,
  `leiras` text COLLATE utf8_hungarian_ci NOT NULL,
  `file` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `nyelv` varchar(5) COLLATE utf8_hungarian_ci NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `oldalak`
--

CREATE TABLE IF NOT EXISTS `oldalak` (
  `id` int(11) NOT NULL,
  `nev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `url` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `tartalom` text COLLATE utf8_hungarian_ci NOT NULL,
  `szulo` int(11) NOT NULL,
  `cim` varchar(560) COLLATE utf8_hungarian_ci NOT NULL,
  `sorrend` int(10) NOT NULL,
  `statusz` int(1) NOT NULL,
  `fokep` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `title` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `keywords` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `description` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `header_custom_code` text COLLATE utf8_hungarian_ci NOT NULL,
  `nyelv` varchar(5) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `oldalak`
--

INSERT INTO `oldalak` (`id`, `nev`, `url`, `tartalom`, `szulo`, `cim`, `sorrend`, `statusz`, `fokep`, `title`, `keywords`, `description`, `header_custom_code`, `nyelv`) VALUES
(52, 'Főoldal', 'fooldal', '', 0, '', 0, 1, '', '', '', '', '', ''),
(53, 'Rólunk', 'rolunk', '<div class="article-header-2">\r\n	<h1>\r\n		A Breitenstein Cukrászda</h1>\r\n</div>\r\n<p>\r\n	Cukrászdánk kínálatában megtalálhatók a hagyományos cukrászsütemények , sós és édes teasütemények, torták, házi sütemények mellett a kézmûves fagylalt is! Ezenkívül esküvõre, rendezvényekre dísztorták készítését vállalunk. Új technológiának köszönhetõen fényképes torták készítését rövid határidõvel vállaljuk.\r\nCukrászatunk nagy figyelmet fordít a higiéniára ezért a Haccp minõségbiztosítási rendszer követelményeit szem elõtt tartva készíti termékeit. </p>\r\n', 0, '', 0, 1, '', '', '', '', '', ''),
(54, 'Galéria', 'galeria', '', 0, '', 0, 1, '', '', '', '', '', ''),
(55, 'Elérhetőségünk', 'elerhetosegunk', '<p>\r\n	J&ouml;jj&ouml;n el szem&eacute;lyesen &eacute;s n&eacute;zze meg nagy &eacute;s vonz&oacute; v&aacute;laszt&eacute;kunkat! &Uuml;zlet&uuml;nkben a s&uuml;tik &ouml;sszes fajt&aacute;j&aacute;t megtal&aacute;lj&aacute;k, a tort&aacute;kt&oacute;l kezdve, a megszokott s&uuml;tem&eacute;nyeken &aacute;t, a szem&eacute;lyre szabott term&eacute;keinkig.</p>\r\n', 0, '', 0, 1, '', '', '', '', '', ''),
(56, 'Termékeink', 'termekek', '', 0, '', 0, 1, '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `slider`
--

CREATE TABLE IF NOT EXISTS `slider` (
  `id` int(11) NOT NULL,
  `sorrend` int(11) NOT NULL,
  `nev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `file` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `nyelv` varchar(5) COLLATE utf8_hungarian_ci NOT NULL,
  `url` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `statusz` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `slider`
--

INSERT INTO `slider` (`id`, `sorrend`, `nev`, `file`, `nyelv`, `url`, `statusz`) VALUES
(1, 1, 'Croissant', 'e4bf7-1.jpg', 'hu', '', 1),
(2, 2, 'Sütemények', 'a1974-2.jpg', 'hu', '', 1),
(3, 3, 'Croissant lekvárral', '96535-3.jpg', 'hu', '', 1);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `termekek`
--

CREATE TABLE IF NOT EXISTS `termekek` (
  `id` int(11) NOT NULL,
  `nev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `url` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `kategoria` int(11) NOT NULL,
  `lead` text COLLATE utf8_hungarian_ci NOT NULL,
  `leiras` text COLLATE utf8_hungarian_ci NOT NULL,
  `fokep` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `title` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `keywords` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `description` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `header_custom_code` text COLLATE utf8_hungarian_ci NOT NULL,
  `nyelv` varchar(5) COLLATE utf8_hungarian_ci NOT NULL,
  `statusz` int(1) NOT NULL,
  `gyarto` int(11) NOT NULL,
  `marka` int(11) NOT NULL,
  `kiemelt` int(11) NOT NULL,
  `ar` int(11) NOT NULL,
  `ar_akcios` int(11) NOT NULL,
  `valuta` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  `ar_beszerzesi` int(11) NOT NULL,
  `suly` int(11) NOT NULL,
  `raktarkeszlet` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `termekek`
--

INSERT INTO `termekek` (`id`, `nev`, `url`, `kategoria`, `lead`, `leiras`, `fokep`, `title`, `keywords`, `description`, `header_custom_code`, `nyelv`, `statusz`, `gyarto`, `marka`, `kiemelt`, `ar`, `ar_akcios`, `valuta`, `ar_beszerzesi`, `suly`, `raktarkeszlet`) VALUES
(1, 'Croissant', '', 3, '', '<p>\r\n	Frissen s&uuml;lt vajas Croissant - Reggelire t&ouml;k&eacute;letes...</p>\r\n', 'b9474-product3.png', '', '', '', '', '', 0, 0, 0, 0, 500, 0, '', 0, 0, 0),
(2, 'Barnakenyér', '', 2, '', '<p>\r\n	A barna keny&eacute;r egyenletes v&eacute;rcukor szintet biztos&iacute;t...</p>\r\n', '6dd21-bread.png', '', '', '', '', '', 0, 0, 0, 0, 200, 0, '', 0, 0, 0),
(3, 'Marcipántorta', '', 1, '', '<p>\r\n	Szem&eacute;lyre szabott marcip&aacute;nnal fedett torta...</p>\r\n', 'af39e-alkalmi-1_01.jpg', '', '', '', '', '', 0, 0, 0, 0, 1000, 0, '', 0, 0, 0);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `termek_kepek`
--

CREATE TABLE IF NOT EXISTS `termek_kepek` (
  `id` int(11) NOT NULL,
  `title` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `file` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `termek` int(11) NOT NULL,
  `sorrend` int(11) NOT NULL,
  `datum` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `termek_tulajdonsagok`
--

CREATE TABLE IF NOT EXISTS `termek_tulajdonsagok` (
  `id` int(11) NOT NULL,
  `termek` int(11) NOT NULL,
  `tulajdonsag_id` int(11) NOT NULL,
  `tulajdonsag` varchar(256) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `tulajdonsag`
--

CREATE TABLE IF NOT EXISTS `tulajdonsag` (
  `id` int(11) NOT NULL,
  `nev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `url` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `szulo` int(200) NOT NULL,
  `nyelv` varchar(5) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `tulajdonsag_kat`
--

CREATE TABLE IF NOT EXISTS `tulajdonsag_kat` (
  `id` int(11) NOT NULL,
  `nev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `nyelv` varchar(256) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL,
  `name` varchar(128) COLLATE utf8_hungarian_ci NOT NULL,
  `password` varchar(128) COLLATE utf8_hungarian_ci NOT NULL,
  `perm` varchar(128) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `users`
--

INSERT INTO `users` (`id`, `name`, `password`, `perm`) VALUES
(2, 'admin', 'c93ccd78b2076528346216b3b2f701e6', '1');

--
-- Indexek a kiírt táblákhoz
--

--
-- A tábla indexei `ajanlat`
--
ALTER TABLE `ajanlat`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `beallitasok`
--
ALTER TABLE `beallitasok`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `companytype`
--
ALTER TABLE `companytype`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `dokumentumok`
--
ALTER TABLE `dokumentumok`
  ADD PRIMARY KEY (`dokumentumokid`);

--
-- A tábla indexei `galeria`
--
ALTER TABLE `galeria`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `gyarto`
--
ALTER TABLE `gyarto`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `gyik`
--
ALTER TABLE `gyik`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `hirek`
--
ALTER TABLE `hirek`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `hirek_kategoria`
--
ALTER TABLE `hirek_kategoria`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `kategoria`
--
ALTER TABLE `kategoria`
  ADD PRIMARY KEY (`kategoriaid`);

--
-- A tábla indexei `kategoriak`
--
ALTER TABLE `kategoriak`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `kerdezzfelelek`
--
ALTER TABLE `kerdezzfelelek`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `marka`
--
ALTER TABLE `marka`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `megjelenes`
--
ALTER TABLE `megjelenes`
  ADD PRIMARY KEY (`megjelenesid`);

--
-- A tábla indexei `nyelvi_forditasok`
--
ALTER TABLE `nyelvi_forditasok`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `oldalak`
--
ALTER TABLE `oldalak`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `termekek`
--
ALTER TABLE `termekek`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `termek_kepek`
--
ALTER TABLE `termek_kepek`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `termek_tulajdonsagok`
--
ALTER TABLE `termek_tulajdonsagok`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `tulajdonsag`
--
ALTER TABLE `tulajdonsag`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `tulajdonsag_kat`
--
ALTER TABLE `tulajdonsag_kat`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- A kiírt táblák AUTO_INCREMENT értéke
--

--
-- AUTO_INCREMENT a táblához `ajanlat`
--
ALTER TABLE `ajanlat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT a táblához `beallitasok`
--
ALTER TABLE `beallitasok`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT a táblához `companytype`
--
ALTER TABLE `companytype`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT a táblához `dokumentumok`
--
ALTER TABLE `dokumentumok`
  MODIFY `dokumentumokid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `galeria`
--
ALTER TABLE `galeria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT a táblához `gyarto`
--
ALTER TABLE `gyarto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `gyik`
--
ALTER TABLE `gyik`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `hirek`
--
ALTER TABLE `hirek`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `hirek_kategoria`
--
ALTER TABLE `hirek_kategoria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `kategoria`
--
ALTER TABLE `kategoria`
  MODIFY `kategoriaid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `kategoriak`
--
ALTER TABLE `kategoriak`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT a táblához `kerdezzfelelek`
--
ALTER TABLE `kerdezzfelelek`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `marka`
--
ALTER TABLE `marka`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `megjelenes`
--
ALTER TABLE `megjelenes`
  MODIFY `megjelenesid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `nyelvi_forditasok`
--
ALTER TABLE `nyelvi_forditasok`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `oldalak`
--
ALTER TABLE `oldalak`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=57;
--
-- AUTO_INCREMENT a táblához `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT a táblához `termekek`
--
ALTER TABLE `termekek`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT a táblához `termek_kepek`
--
ALTER TABLE `termek_kepek`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `termek_tulajdonsagok`
--
ALTER TABLE `termek_tulajdonsagok`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `tulajdonsag`
--
ALTER TABLE `tulajdonsag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `tulajdonsag_kat`
--
ALTER TABLE `tulajdonsag_kat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
